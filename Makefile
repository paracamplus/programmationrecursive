# Time-stamp: "2021-03-19 10:42:59 queinnec"

work : nothing 
clean :: cleanMakefile

deploy : .gitlab-ci.yml
	git commit -m "deployed on $$(date -u --iso-8601=seconds)" .
	git push

# 2021 mar 19:
# MOOC is on https://programmation-recursive-2.appspot.com/
# Teaser page is on https://programmation-recursive.net/
# Repository is https://gitlab.com/paracamplus/programmationrecursive
# Doc https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/
# Pages https://paracamplus.gitlab.io/programmationrecursive
# DNS programmation-recursive.net   A 35.185.44.232
#     www 1800 IN CNAME paracamplus.gitlab.io.

# #################################### Obsolete
REMOTE  =   programmation-recursive.net
install :
	rsync -avuL . ${REMOTE}:/var/www/${REMOTE}/

# end of Makefile
